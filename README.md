# Docs for Document Template System (DTS)

## DTS RESTful API
API documention to manage and render document templates.

You can copy the contents of dts-documentation.yml and paste it into editor.swagger.io to view the rendered output.

Link for Swagger documentation on Github.
https://homeceu.github.io/dts-docs/#
